Vue.component('pagination',{
	props: ['stuff'],
	template: 	`
					<div class='basicBorder paginationContainer'>
						<div class='rgbList basicBorder' v-bind:style='"grid-template-rows: repeat("+this.itemsPerPage+", 1fr);"'>
							<rgb v-for='rgb in itemsToDisplay' v-bind:rgb=rgb v-bind:key=stuff.id></rgb>
						</div>
						<div class='basicBorder pageController'>
							<div class='lArrow centeredText' v-on:click='prevPage'><==</div>
							<div class='pageTracker centeredText'>{{currentPage}}/{{totalPages}}</div>
							<div class='rArrow centeredText' v-on:click='nextPage'>==></div>
						</div>
					</div>
				`,
	data: function(){
		return {
			itemCount: this.stuff.length,
			itemsPerPage: 3,
			currentPage: 1
		}
	},
	computed: {
		itemsToDisplay: function(){
			startPoint = ((this.currentPage*this.itemsPerPage)-this.itemsPerPage);
			endPoint = (this.currentPage*this.itemsPerPage);
			return this.stuff.slice(startPoint,endPoint);//splice doesn't care if the endpoint is greater than the length of the array
		},
		totalPages: function(){
			pages = this.itemCount/this.itemsPerPage;
			if (pages%1>0){
				pages=pages+1;
			}
			return Math.floor(pages);
		}
	},
	methods: {
		nextPage: function(){
			if (this.currentPage < this.totalPages){
				this.currentPage = this.currentPage+1;
			}else{
				console.log('already at end');
			}
		},
		prevPage: function(){
			if (this.currentPage > 1){
				this.currentPage = this.currentPage-1;
			}else{
				console.log('already at start');
			}
		}
	}
})

Vue.component('rgb',{
	props: ['rgb'],
	template: 	`
				<div class='basicBorder rgbCard'>
					<div class='centeredText'>{{rgb.title}}</div>
					<div class='centeredText rgbShow' v-bind:style='"background-color: rgb"+this.rgb.rgb'>{{rgb.rgb}}</div>
				</div>
				`
})

new Vue({
	el: '#vue-app',
	template:	`
					<div class='mainContainer basicBorder'>
						<pagination v-bind:stuff=stuff></pagination>
					</div>	
				`,
	data: {
		stuff: [],
	},
	methods: {
		intializeStuff: function(numberOfStuff){
			for(var thing = 0; thing < numberOfStuff; thing++){
				var randomRGB = {
					id: thing,
					title: 'RGB '+(thing+1),
					rgb: '('+this.generateRandomBetween(0,256)+','+this.generateRandomBetween(0,256)+','+this.generateRandomBetween(0,256)+')',
				}
				this.stuff.push(randomRGB);
				console.log(randomRGB.title);
				console.log(randomRGB.rgb);
			}
		},
		generateRandomBetween: function(min, max){
			return Math.floor(Math.random() * (max - min));
		}
	},
	created: function(){
		this.intializeStuff(7);
	}
})